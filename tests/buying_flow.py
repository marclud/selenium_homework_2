import pytest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

from models.cart_page import CartPage
from models.main_page import MyStore
from models.product_page import ProductPage
from models.search_result_page import SearchResultPage
from tests.test_data import base_url


@pytest.fixture()
def driver():
    driver = webdriver.Chrome(executable_path=ChromeDriverManager().install())

    driver.get(base_url)

    yield driver

    driver.quit()


def test_adding_new_item_to_basket(driver):
    my_store = MyStore(driver)
    my_store.verify_if_page_open()
    my_store.use_search_button('Printed Summer')

    search_result_page = SearchResultPage(driver)
    search_result_page.verify_if_page_open()
    search_result_page.add_product_to_basket()

    cart_page = CartPage(driver)
    cart_page.verify_if_page_open('Printed Summer')
