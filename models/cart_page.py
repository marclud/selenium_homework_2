from selenium.webdriver import Chrome, ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

class CartPage:
    def __init__(self, driver: Chrome):
        self.driver = driver
        self.checkout_button_selector = (By.XPATH, "//a[@title='Proceed to checkout']")
        self.product_description_selector = (By.CSS_SELECTOR, ".cart_description > .product-name  > a")

    def verify_if_page_open(self, searched_phrase):
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(self.checkout_button_selector)).click()
        product_description_element = self.driver.find_element(*self.product_description_selector).text

        assert searched_phrase in product_description_element