from selenium.webdriver import Chrome, ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

class SearchResultPage:
    def __init__(self, driver: Chrome):
        self.driver = driver
        self.product_container_selector = (By.CLASS_NAME, 'product-container')
        self.add_to_cart_selector = (By.CSS_SELECTOR, ".ajax_add_to_cart_button")


    def verify_if_page_open(self):
        WebDriverWait(self.driver, 10).until(expected_conditions.presence_of_element_located(self.product_container_selector))

    def add_product_to_basket(self):
        element = self.driver.find_element(*self.product_container_selector)

        hover = ActionChains(self.driver).move_to_element(element)
        hover.perform()
        self.driver.find_element(*self.add_to_cart_selector).click()
