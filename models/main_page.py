from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class MyStore:

    def __init__(self, driver: Chrome):
        self.driver = driver
        self.homepage_slider_area = (By.ID, 'homepage-slider')
        self.search_field_selector = (By.ID, 'search_query_top')
        self.search_button_selector = (By.CLASS_NAME, 'button-search')
        self.product_container = (By.CLASS_NAME, 'product_img_link')

    def verify_if_page_open(self):
        WebDriverWait(self.driver, 10).until(expected_conditions.presence_of_element_located(self.homepage_slider_area))

    def use_search_button(self, searched_phrase):
        self.driver.find_element(*self.search_field_selector).send_keys(searched_phrase)
        self.driver.find_element(*self.search_button_selector).click()

    def go_to_product_page(self):
        self.driver.find_element(*self.product_container).click()
