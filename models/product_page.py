from selenium.webdriver import Chrome, ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class ProductPage:

    def __init__(self, driver: Chrome):
        self.driver = driver
        self.add_to_cart_button = (By.CSS_SELECTOR, '#add_to_cart .exclusive')
        self.checkout_button = (By.CSS_SELECTOR, '#layer_cart .button-medium')

    def verify_if_page_open(self):
        WebDriverWait(self.driver, 10).until(expected_conditions.presence_of_element_located(self.add_to_cart_button))

    def adding_object_to_cart(self):
        self.driver.find_element(*self.add_to_cart_button).click()

    def go_to_checkout(self):
        WebDriverWait(self.driver, 10).until(expected_conditions.element_to_be_clickable(self.checkout_button))
        self.driver.find_element(*self.checkout_button).click()
